package sdk

import (
	"fmt"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/validation"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/terraform-provider-gitlab/internal/provider/utils"
)

func gitlabProjectNotificationSettingsGetSchema() map[string]*schema.Schema {
	return map[string]*schema.Schema{
		"level": {
			Description: "The global notification level.",
			Type:        schema.TypeString
		},
		"close_issue": {
			Description: "Whether or not to send notifications when an issue is closed.",
			Type:        schema.TypeBool,
		},
		"close_merge_request": {
			Description: "Whether or not to send notifications when a merge request is closed.",
			Type:        schema.TypeBool,
		},
		"failed_pipeline": {
			Description: "Whether or not to send notifications when a pipeline fails.",
			Type:        schema.TypeBool,
		},
		"merge_merge_request": {
			Description: "Whether or not to send notifications when a merge request gets merged.",
			Type:        schema.TypeBool,
		},
		"new_issue": {
			Description: "Whether or not to send notifications when an issue is created.",
			Type:        schema.TypeBool,
		},
		"new_note": {
			Description: "Whether or not to send notifications when a note is created.",
			Type:        schema.TypeBool,
		},
		"new_merge_request": {
			Description: "Whether or not to send notifications when a merge request is created.",
			Type:        schema.TypeBool,
		},
		"reassign_issue": {
			Description: "Whether or not to send notifications when an issue is reassigned.",
			Type:        schema.TypeBool,
		},
		"reassign_merge_request": {
			Description: "Whether or not to send notifications when a merge request is reassigned.",
			Type:        schema.TypeBool,
		},
		"reopen_issue": {
			Description: "Whether or not to send notifications when an issue is reopened.",
			Type:        schema.TypeBool,
		},
		"reopen_merge_request": {
			Description: "Whether or not to send notifications when a merge request is reopened.",
			Type:        schema.TypeBool,
		},
		"success_pipeline": {
			Description: "Whether or not to send notifications when a pipeline succeeds.",
			Type:        schema.TypeBool,
		}
	}
}