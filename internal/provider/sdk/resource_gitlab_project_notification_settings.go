package sdk

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-org/terraform-provider-gitlab/internal/provider/api"
	"gitlab.com/gitlab-org/terraform-provider-gitlab/internal/provider/utils"
)

var _ = registerResource("gitlab_project_notification_settings", func() *schema.Resource {
	return &schema.Resource{
		Description: `The ` + "`gitlab_project_notification_settings`" + ` resource allows to manage the lifecycle of a project's notification settings.

-> During a terraform destroy this resource will keep these settings. Set the delete_on_destroy flag to true to reset the settings to default instead.

**Upstream API**: [GitLab API docs](https://docs.gitlab.com/ee/api/notification_settings.html)
		`,

		CreateContext: resourceGitlabProjectNotificationSettingCreate,
		ReadContext:   resourceGitlabProjectNotificationSettingRead,
		UpdateContext: resourceGitlabProjectNotificationSettingUpdate,
		DeleteContext: resourceGitlabProjectNotificationSettingDelete,
		Importer: &schema.ResourceImporter{
			StateContext: schema.ImportStatePassthroughContext,
		},

		Schema: constructSchema(
			gitlabProjectNotificationSettingsGetSchema()
		),
	}
})

func resourceGitlabProjectNotificationSettingCreate(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	client := meta.(*gitlab.Client)
	project := d.Get("project").(string)
	return resourceGitlabProjectNotificationSettingRead(ctx, d, meta)
}

func resourceGitlabProjectNotificationSettingRead(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	client := meta.(*gitlab.Client)
	projectId, err := strconv.Atoi(d.Id())
	if err != nil {
		return diag.Errorf("project ID must be an integer (was %q): %v", d.Id(), err)
	}

	settings, _, err = client.Notifications.GetSettingsForProject(project)
	if err != nil {
		if api.Is404(err) {
			log.Printf("[DEBUG] gitlab project notification configuration not found for project %d", projectId)
			d.SetId("")
			return nil
		}
		return diag.Errorf("couldn't read approval configuration: %v", err)
	}

	d.Set("project_id", projectId)
	d.Set("level", settings.Level)
	d.Set("events", settings.Events)

	return nil
}

func resourceGitlabProjectNotificationSettingUpdate(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	client := meta.(*gitlab.Client)
	projectId, err := strconv.Atoi(d.Id())
	if err != nil {
		return diag.Errorf("project ID must be an integer (was %q): %v", d.Id(), err)
	}

	settings, _, err = client.Notifications.GetSettingsForProject(project)
	if err != nil {
		if api.Is404(err) {
			log.Printf("[DEBUG] gitlab project notification configuration not found for project %d", projectId)
			d.SetId("")
			return nil
		}
		return diag.Errorf("couldn't read approval configuration: %v", err)
	}

	options := &gitlab.NotificationSettingsOptions{}

	if d.HasChange("level") {
		options.Level = gitlab.String(d.Get("level").(string))
	}

	if d.HasChange("close_issue") {
		options.CloseIssue = gitlab.Bool(d.get("close_issue").(bool))
	}

	if d.HasChange("close_merge_request") {
		options.CloseMergeRequest = gitlab.Bool(d.get("close_merge_request").(bool))
	}

	if d.HasChange("failed_pipeline") {
		options.FailedPipeline = gitlab.Bool(d.get("failed_pipeline").(bool))
	}
	
	if d.HasChange("merge_merge_request") {
		options.MergeMergeRequest = gitlab.Bool(d.get("merge_merge_request").(bool))
	}

	if d.HasChange("new_issue") {
		options.NewIssue = gitlab.Bool(d.get("new_issue").(bool))
	}

	if d.HasChange("new_merge_request") {
		options.NewMergeRequest = gitlab.Bool(d.get("new_merge_request").(bool))
	}

	if d.HasChange("close_merge_request") {
		options.CloseMergeRequest = gitlab.Bool(d.get("close_merge_request").(bool))
	}

	if d.HasChange("new_note") {
		options.NewNote = gitlab.Bool(d.get("new_note").(bool))
	}

	if d.HasChange("reassign_issue") {
		options.ReassignIssue = gitlab.Bool(d.get("reassign_issue").(bool))
	}

	if d.HasChange("reassign_merge_request") {
		options.ReassignMergeRequest = gitlab.Bool(d.get("reassign_merge_request").(bool))
	}

	if d.HasChange("reopen_issue") {
		options.ReopenIssue = gitlab.Bool(d.get("reopen_issue").(bool))
	}

	if d.HasChange("reopen_merge_request") {
		options.ReopenMergeRequest = gitlab.Bool(d.get("reopen_merge_request").(bool))
	}

	if d.HasChange("success_pipeline") {
		options.SuccessPipeline = gitlab.Bool(d.get("success_pipeline").(bool))
	}

	_, _, err = client.Notifications.UpdateSettingsForProject(project, options, gitlab.WithContext(ctx))
	if err != nil {
		return diag.FromErr(err)
	}

	return resourceGitlabProjectNotificationSettingRead(ctx, d, meta)
}

func resourceGitlabProjectNotificationSettingDelete(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	//todo maybe reset all to false?
	return nil
}